package com.youlai.hazelcast.config;

import com.hazelcast.core.HazelcastInstance;
import com.youlai.hazelcast.core.base.DistributedCountDownLatchFactory;
import com.youlai.hazelcast.core.base.DistributedLockFactory;
import com.youlai.hazelcast.core.base.DistributedMapFactory;
import com.youlai.hazelcast.core.base.DistributedQueueFactory;
import com.youlai.hazelcast.core.base.DistributedSetFactory;
import com.youlai.hazelcast.core.support.HazelcastCountDownLatchFactory;
import com.youlai.hazelcast.core.support.HazelcastLockFactory;
import com.youlai.hazelcast.core.support.HazelcastMapFactory;
import com.youlai.hazelcast.core.support.HazelcastProperties;
import com.youlai.hazelcast.core.support.HazelcastQueueFactory;
import com.youlai.hazelcast.core.support.HazelcastSetFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author Fxz
 * @version 0.0.1
 * @date 2023/3/26 09:44
 */
@EnableConfigurationProperties(HazelcastProperties.class)
@Configuration
public class HazelcastBaseAutoConfiguration {

	@Bean
	DistributedCountDownLatchFactory distributedCountDownLatchFactory(
			@Qualifier("hazelcastInstance") HazelcastInstance hazelcastInstance) {
		return new HazelcastCountDownLatchFactory(hazelcastInstance);
	}

	@Bean
	DistributedLockFactory distributedLockFactory(@Qualifier("hazelcastInstance") HazelcastInstance hazelcastInstance) {
		return new HazelcastLockFactory(hazelcastInstance);
	}

	@Bean
	DistributedMapFactory distributedMapFactory(@Qualifier("hazelcastInstance") HazelcastInstance hazelcastInstance) {
		return new HazelcastMapFactory(hazelcastInstance);
	}

	@Bean
	DistributedQueueFactory distributedQueueFactory(
			@Qualifier("hazelcastInstance") HazelcastInstance hazelcastInstance) {
		return new HazelcastQueueFactory(hazelcastInstance);
	}

	@Bean
	DistributedSetFactory distributedSetFactory(@Qualifier("hazelcastInstance") HazelcastInstance hazelcastInstance) {
		return new HazelcastSetFactory(hazelcastInstance);
	}

}