
package com.youlai.hazelcast.config;

import com.hazelcast.core.HazelcastInstance;
import com.youlai.hazelcast.core.base.DistributedCacheProvider;
import com.youlai.hazelcast.core.cache.DefaultCacheManager;
import com.youlai.hazelcast.core.cache.HazelcastCacheProvider;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author Fxz
 * @version 0.0.1
 * @date 2023/3/26 09:44
 */
@Configuration
public class HazelcastCacheAutoConfiguration {

    @Bean
    DistributedCacheProvider hazelcastCacheProvider(
            @Qualifier("hazelcastInstance") HazelcastInstance hazelcastInstance) {
        return new HazelcastCacheProvider(hazelcastInstance);
    }

    @Bean
    DefaultCacheManager defaultCacheManager(DistributedCacheProvider distributedCacheProvider) {
        return new DefaultCacheManager(distributedCacheProvider);
    }

}
