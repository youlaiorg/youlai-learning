package com.youlai.hazelcast.demo;

/**
 * @author Ray
 **/
public interface IResultCode {

    String getCode();

    String getMsg();

}
