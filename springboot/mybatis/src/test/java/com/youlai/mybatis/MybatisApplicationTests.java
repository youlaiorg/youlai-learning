package com.youlai.mybatis;

import com.youlai.mybatis.DTO.SysUserDTO;
import com.youlai.mybatis.domain.SysUser;
import com.youlai.mybatis.mapper.SysDeptMapper;
import com.youlai.mybatis.mapper.SysUserMapper;
import com.youlai.mybatis.service.SysUserService;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;

@SpringBootTest
@Slf4j
class MybatisApplicationTests {

    @Resource
    private SysUserMapper sysUserMapper;

    @Resource
    private SysDeptMapper sysDeptMapper;
    @Resource
    private SysUserService sysUserService;

    /**
     * 测试一级缓存
     * mybatis.configuration.local-cache-scope=session
     * 默认开启，但是需要开启事务,保证在一个sqlSession中才能使一级缓存生效
     */
    @Test
    @Transactional(rollbackFor = Throwable.class)
    void oneSqlSessionCache() {
        SysUser sysUser = sysUserMapper.selectByPrimaryKey(1L);
        sysUserMapper.selectByPrimaryKey(1L);
        sysUserMapper.selectByPrimaryKey(1L);
        sysUserMapper.selectByPrimaryKey(1L);
        System.out.println(sysUser);
    }

    /**
     * 测试二级缓存
     * 关闭一级缓存:mybatis.configuration.local-cache-scope=STATEMENT
     * 开启二级缓存:mybatis.configuration.cache-enabled=true, <cache type="com.youlai.mybatis.service.MyCustomCache"/>
     * 注意自定义缓存需要实现Serializable接口
     */
    @Test
    void doubleSqlSessionCache() {
        SysUser sysUser = sysUserMapper.selectByPrimaryKey(1L);
        sysUserMapper.selectByPrimaryKey(1L);
        sysUserMapper.selectByPrimaryKey(1L);
        sysUserMapper.selectByPrimaryKey(1L);
        System.out.println(sysUser);
    }

    /**
     * 脏读复现
     * 在一个事务中读取了另一个事务未提交的数据，如果未提交的事务回滚，则读取到的数据就是无效(脏)的
     * 解决方案：设置隔离级别为Isolation.READ_COMMITTED，同时要关闭一级缓存
     * 设置Isolation.READ_COMMITTED隔离级别但不关闭一级缓存，后面两次都是缓存里面的脏数据
     * 关闭一级缓存，但设置READ_UNCOMMITTED，第二次是脏数据，第三次是正确的数据
     */
    @Transactional(propagation = Propagation.REQUIRED,isolation = Isolation.READ_UNCOMMITTED)
    @Test
    void dirtyRead() throws InterruptedException {
        SysUser sysUser = sysUserMapper.selectByPrimaryKey(1L);
        log.info("第一次读:{}", sysUser.getNickname());
        new Thread(()->{
        sysUserService.updateError(sysUser);
        }).start();
        Thread.sleep(500);
        log.info("开始查询第二次");
        SysUser sysUser2 = sysUserMapper.selectByPrimaryKey(1L);
        log.info("第二次读:{}", sysUser2.getNickname());
        Thread.sleep(3000);
        log.info("开始第三次读");
        SysUser sysUser3 = sysUserMapper.selectByPrimaryKey(1L);
        log.info("第三次读:{}", sysUser3.getNickname());
    }


    /**
     * 不可重复读复现
     * 在同一事务中，读取同一条记录两次,理想情况两次读取结果是一致的，但是在两次读取之间，另一个事务修改了这条记录，导致两次读取的结果不一致
     * 解决方法:设置隔离级别为REPEATABLE_READ,这种虽然两次读的数据一样，但是第二次数据符合实际数据,实际场景应该使用悲观锁或乐观锁来解决
     * 不可重复读重点在于数据是否被改变了
     */
    @Test
    @Transactional(rollbackFor = Throwable.class,isolation = Isolation.READ_COMMITTED)
    void uniqueReader() throws InterruptedException {
        SysUser sysUser = sysUserMapper.selectByPrimaryKey(1L);
        log.info("第一次读:{}", sysUser.getNickname());
        sysUserService.updateSysUser(sysUser);
        Thread.sleep(1000);
        SysUser sysUser2 = sysUserMapper.selectByPrimaryKey(1L);
        log.info("第二次读:{}", sysUser2.getNickname());
    }


    /**
     * 幻读复现
     * 在同一事务中，读取了符合某些特定条件的记录集合，但是在事务执行过程中,
     * 另一个事务插入了符合该条件的新记录,导致第二次读取时，结果集合发生了变化
     * 幻读重点在于数据是否存在。原本不存在的数据却真实的存在了
     * A查询不到这个数据，不代表这个数据不存在。查询得到了某条数据，不代表它真的存在。这样是是而非的查询，就像是幻觉一样，似真似假，故为幻读。
     */
    @Transactional(propagation = Propagation.REQUIRED,isolation = Isolation.READ_COMMITTED)
    @Test
    void phantomRead() throws InterruptedException {
        int startId = 100,endId=110;
        //删除幻读数据
        sysUserService.resetSysUser(startId,endId);
        //第一次读
        List<SysUser> usersByIdRange = sysUserMapper.getUsersByIdRange(startId, endId);
        log.info("第一次读取{}条",usersByIdRange.size());
        //添加幻读数据
        new Thread(()->{
            sysUserService.addSysUser(startId,endId);
        }).start();
        Thread.sleep(1000);
        //第二次读
        List<SysUser> usersByIdRange1 = sysUserMapper.getUsersByIdRange(startId, endId);
        log.info("第二次读取{}条",usersByIdRange1.size());
    }


    public void addSysUser(int startId, int endId){
        log.info("添加数据");
        for (Integer i = startId; i < endId-5; i++) {
            SysUser sysUser = new SysUser();
            sysUser.setId(i);
            sysUser.setNickname("幻读用户"+i);
            sysUser.setUsername("幻读用户"+i);
            sysUserMapper.insert(sysUser);
        }
    }

    /**
     * 一对一嵌套查询
     */
    @Test
    void nestedQueryResultTest() {
        SysUserDTO sysUserDTO = sysUserMapper.selectUserDTOByPrimaryKey(2L);
       log.info(sysUserDTO.toString());
    }


    /**
     * 插件测试
     */
    @Test
    void plugin(){
        SysUser sysUser = sysUserMapper.selectByPrimaryKey(1L);
        log.info(sysUser.toString());
    }

}
