package com.youlai.mybatis.mapper;

import com.youlai.mybatis.domain.SysDictItem;
import org.apache.ibatis.annotations.Mapper;

/**
* @author Administrator
* @description 针对表【sys_dict_item(字典数据表)】的数据库操作Mapper
* @createDate 2023-03-18 21:22:07
* @Entity com.youlai.mybatis.domain.SysDictItem
*/
@Mapper
public interface SysDictItemMapper {

    int deleteByPrimaryKey(Long id);

    int insert(SysDictItem record);

    int insertSelective(SysDictItem record);

    SysDictItem selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(SysDictItem record);

    int updateByPrimaryKey(SysDictItem record);

}
