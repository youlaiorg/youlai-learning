package com.youlai.mybatis.mapper;

import com.youlai.mybatis.domain.SysMenu;
import org.apache.ibatis.annotations.Mapper;

/**
* @author Administrator
* @description 针对表【sys_menu(菜单管理)】的数据库操作Mapper
* @createDate 2023-03-18 21:22:07
* @Entity com.youlai.mybatis.domain.SysMenu
*/
@Mapper
public interface SysMenuMapper {

    int deleteByPrimaryKey(Long id);

    int insert(SysMenu record);

    int insertSelective(SysMenu record);

    SysMenu selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(SysMenu record);

    int updateByPrimaryKey(SysMenu record);

}
