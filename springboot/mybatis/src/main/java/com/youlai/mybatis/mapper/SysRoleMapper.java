package com.youlai.mybatis.mapper;

import com.youlai.mybatis.domain.SysRole;
import org.apache.ibatis.annotations.Mapper;

/**
* @author Administrator
* @description 针对表【sys_role(角色表)】的数据库操作Mapper
* @createDate 2023-03-18 21:22:07
* @Entity com.youlai.mybatis.domain.SysRole
*/
@Mapper
public interface SysRoleMapper {

    int deleteByPrimaryKey(Long id);

    int insert(SysRole record);

    int insertSelective(SysRole record);

    SysRole selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(SysRole record);

    int updateByPrimaryKey(SysRole record);

}
