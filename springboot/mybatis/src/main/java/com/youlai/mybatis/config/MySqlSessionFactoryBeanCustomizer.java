package com.youlai.mybatis.config;

import com.youlai.mybatis.service.MyCustomCache;
import com.youlai.mybatis.service.MyStringTypeHandler;
import com.youlai.mybatis.service.MybatisPlugin;
import org.apache.ibatis.session.Configuration;
import org.mybatis.spring.SqlSessionFactoryBean;
import org.mybatis.spring.boot.autoconfigure.ConfigurationCustomizer;
import org.mybatis.spring.boot.autoconfigure.SqlSessionFactoryBeanCustomizer;
import org.springframework.stereotype.Component;

/**
 * @author zc
 * @date 2023-03-18 23:32
 */
@Component
public class MySqlSessionFactoryBeanCustomizer implements SqlSessionFactoryBeanCustomizer, ConfigurationCustomizer {

    MyCustomCache cache = new MyCustomCache("com.youlai.mybatis.service.MyCustomCache");

    @Override
    public void customize(SqlSessionFactoryBean factoryBean) {
        //注册插件
        factoryBean.setPlugins(new MybatisPlugin());

    }

    @Override
    public void customize(Configuration configuration) {
//        configuration.setCacheEnabled(true);
        configuration.addCache(cache);
        configuration.getTypeHandlerRegistry().register(String.class,new MyStringTypeHandler());
    }
}

