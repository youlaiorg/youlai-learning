package com.youlai.mybatis.mapper;

import com.youlai.mybatis.DTO.SysUserDTO;
import com.youlai.mybatis.domain.SysUser;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.cache.annotation.CacheConfig;

import java.util.List;

/**
* @author Administrator
* @description 针对表【sys_user(用户信息表)】的数据库操作Mapper
* @createDate 2023-03-18 21:22:07
* @Entity com.youlai.mybatis.domain.SysUser
*/
@Mapper
//@CacheNamespace(implementation = MyCustomCache.class)
@CacheConfig(cacheNames = "com.youlai.mybatis.service.MyCustomCache")
public interface SysUserMapper {

    int deleteByPrimaryKey(Long id);

    int insert(SysUser record);

    int insertSelective(SysUser record);

//    @Select("SELECT * FROM sys_user WHERE id = #{id}")
//    @Options(useCache = true)
    SysUser selectByPrimaryKey(Long id);


    @Options(useCache = true)
    @Select("SELECT * FROM sys_user WHERE id BETWEEN #{startId} AND #{endId}")
    List<SysUser> getUsersByIdRange(@Param("startId") int startId, @Param("endId") int endId);


    SysUserDTO selectUserDTOByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(SysUser record);

    int updateByPrimaryKey(SysUser record);

}
