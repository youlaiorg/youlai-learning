package com.youlai.webflux;

import org.springframework.util.StopWatch;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

/**
 *
 * @author zc
 * @date 2023-03-27 21:39
 */
@RestController
public class TestWebFluxController {

    /**
     * Mono表示包含零个或一个元素的异步序列
     */
    @GetMapping("/mono")
    public Mono<String> mono()  {
        StopWatch stopWatch =    new StopWatch();
        stopWatch.start();
        Mono<String> hello = Mono.fromSupplier(() -> getHelloStr());
        stopWatch.stop();
        System.out.println("接口耗时："+stopWatch.getLastTaskTimeMillis());
        return hello;
    }



    private String getHelloStr() {
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return "hello";
    }

    /**
     * Flux表示包含零个或多个元素的异步序列
     */
    @GetMapping("/flux")
    public Flux<String> flux() {
        StopWatch stopWatch =    new StopWatch();
        stopWatch.start();
        Flux<String> flux = Flux.fromArray(new String[]{"小黑","小胖","小六","一鑫"}).map(s -> {
            try {
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            return "二班：" + s;
        });
        stopWatch.stop();
        System.out.println("接口耗时："+stopWatch.getLastTaskTimeMillis());
        return flux;
    }

}
