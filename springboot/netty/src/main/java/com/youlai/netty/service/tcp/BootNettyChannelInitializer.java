package com.youlai.netty.service.tcp;


import com.youlai.netty.service.handler.TCPHandler;
import io.netty.channel.Channel;
import io.netty.channel.ChannelHandler;
import io.netty.channel.ChannelInitializer;
import io.netty.handler.timeout.IdleStateHandler;

import java.util.concurrent.TimeUnit;


/**
 * 通道初始化
 * @author zc
 * @date 2023-01-30 22:11
 */
@ChannelHandler.Sharable
public class BootNettyChannelInitializer<SocketChannel> extends ChannelInitializer<Channel> {


    public static long READ_TIME_OUT = 300;

    public static long WRITE_TIME_OUT = 300;

    public static long ALL_TIME_OUT = 600;

    @Override
    protected void initChannel(Channel ch) throws Exception {

        ch.pipeline().addLast(new IdleStateHandler(READ_TIME_OUT, WRITE_TIME_OUT, ALL_TIME_OUT, TimeUnit.SECONDS));

//         字符编解码器
//        ch.pipeline().addLast("encoder", new StringEncoder(CharsetUtil.UTF_8));
//        ch.pipeline().addLast("decoder", new StringDecoder(CharsetUtil.UTF_8));
        /**
         * 换行编解码器
         */
//        ch.pipeline().addLast("encoder",new LineEncoder());
//        ch.pipeline().addLast("decoder",new LineEncoder());

        /**
         * 自定义编解码器
         */
          ch.pipeline().addLast(new MyMessageDecoder(),new MyMessageEncoder());
          //心跳handler
        ch.pipeline().addLast(new PingPangHandler());
        //处理正常数据handler
        ch.pipeline().addLast(new BootNettyChannelInboundHandlerAdapter());
        ch.pipeline().addLast(new TCPHandler());


    }
}
