package com.youlai.netty.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2WebMvc;

/**
 * @author zc
 * @date 2023-01-15 16:46
 */
@Configuration
@EnableSwagger2WebMvc
public class Knife4jConfiguration {

    @Bean(value = "defaultApi2")
    public Docket defaultApi2() {
        Docket docket=new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(new ApiInfoBuilder()
                        .description("# 有来学习API")
                        .termsOfServiceUrl("http://127.0.0.1:8080/")
                        .contact(new Contact("zc","https://www.cnblogs.com/chuan2/","2256222053@qq.com"))
                        .version("1.0")
                        .build())
                //分组名称
                .groupName("netty")
                .select()
                //这里指定Controller扫描包路径
                .apis(RequestHandlerSelectors.basePackage("com.youlai.netty.controller"))
                .paths(PathSelectors.any())
                .build();
        return docket;
    }
}