package com.youlai.netty.service.handler;

import com.youlai.netty.service.tcp.MyMessageProtocol;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import io.netty.handler.codec.mqtt.MqttMessage;
import lombok.extern.slf4j.Slf4j;

/**
 * @author zc
 * @date 2023-02-04 22:08
 */
@Slf4j
public class TCPHandler extends SimpleChannelInboundHandler<MyMessageProtocol> {

    @Override
    protected void channelRead0(ChannelHandlerContext ctx, MyMessageProtocol msg) throws Exception {
        log.info("handler2处理:{}消息:{}",ctx.channel().id().asShortText(),msg);
    }
}
