package com.youlai.netty.controller;



import com.youlai.netty.service.tcp.BootNettyChannel;
import com.youlai.netty.service.tcp.BootNettyChannelCache;
import io.netty.buffer.Unpooled;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * @author zc
 * @date 2023-01-30 22:14
 */

@RestController
public class BootNettyController {

    @GetMapping(value = {"", "/"})
    public String index() {
        return "netty springBoot tcp demo";
    }

    @ApiOperation(value = "查询客户端code")
    @GetMapping("/clientList")
    public List<Map<String, String>> clientList() {
        List<Map<String, String>> list = new ArrayList<>();
        for (Map.Entry<String, BootNettyChannel> entry : BootNettyChannelCache.channelMapCache.entrySet()) {
            Map<String, String> map = new HashMap<String, String>();
            map.put("code", entry.getKey());
            //map.put("code", entry.getValue().getCode());
            map.put("report_last_data", entry.getValue().getReport_last_data());
            list.add(map);
        }
        return list;
    }

    @ApiOperation(value = "发送消息给所有客户端")
    @PostMapping("/downDataToAllClient")
    public String downDataToAllClient(@RequestParam(name = "content", required = true) String content) {
        for (Map.Entry<String, BootNettyChannel> entry : BootNettyChannelCache.channelMapCache.entrySet()) {
            BootNettyChannel bootNettyChannel = entry.getValue();
            if (bootNettyChannel != null && bootNettyChannel.getChannel().isOpen()) {
                bootNettyChannel.getChannel().writeAndFlush(Unpooled.buffer().writeBytes(content.getBytes()));
                // netty的编码已经指定，因此可以不需要再次确认编码
                // bootNettyChannel.getChannel().writeAndFlush(Unpooled.buffer().writeBytes(content.getBytes(CharsetUtil.UTF_8)));
            }
        }
        return "ok";
    }

    @ApiOperation(value = "发送消息给指定code的客户端")
    @PostMapping("/downDataToClient")
    public String downDataToClient(@RequestParam(name = "code", required = true) String code, @RequestParam(name = "content", required = true) String content) {
        BootNettyChannel bootNettyChannel = BootNettyChannelCache.get(code);
        if (bootNettyChannel != null && bootNettyChannel.getChannel().isOpen()) {
            bootNettyChannel.getChannel().writeAndFlush(Unpooled.buffer().writeBytes(content.getBytes()));
            // netty的编码已经指定，因此可以不需要再次确认编码
            // bootNettyChannel.getChannel().writeAndFlush(Unpooled.buffer().writeBytes(content.getBytes(CharsetUtil.UTF_8)));
            return "success";
        }
        return "fail";
    }

}