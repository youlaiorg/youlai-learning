package com.youlai.netty.service.tcp;

import lombok.Data;

/**
 * 协议包
 * @author zc
 * @date 2023-01-30 23:48
 */
@Data
public class MyMessageProtocol {

    private Integer id;

    private String time;

    private String body;

}
