package com.youlai.netty.config;

import cn.hutool.core.thread.ThreadUtil;
import com.youlai.netty.service.tcp.client.TCPClient;
import com.youlai.netty.service.tcp.server.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

/**
 * 启动服务器
 * @author zc
 * @date 2023-01-30 22:04
 */
@Slf4j
@Component
public class NettyConfig  {

    private int TCP_SERVER_PORT = 5600;
    private int UDP_SERVER_PORT = 5601;
    private int HTTP_SERVER_PORT = 5602;
    private int WEBSOCKET_SERVER_PORT = 5603;
    private int MQTT_SERVER_PORT = 5604;
    private String host = "127.0.0.1";


    @PostConstruct
    @Order(1)
    public void createTCPServer() {
        ThreadUtil.execAsync(()->{
            new TCPServer().bind(TCP_SERVER_PORT);
        });
    }

    @Order(2)
    @PostConstruct
    public void createUDPServer(){
        ThreadUtil.execAsync(()->{
            new UDPServer().bind(UDP_SERVER_PORT);
        });
    }

    @Order(3)
    @PostConstruct
    public void createHTTPServer(){
        ThreadUtil.execAsync(()->{
            new HTTPServer().bind(HTTP_SERVER_PORT);
        });
    }


    @Order(4)
    @PostConstruct
    public void createWebSocketServer(){
        ThreadUtil.execAsync(()->{
            new WebSocketServer().bind(WEBSOCKET_SERVER_PORT);
        });
    }

    @Order(5)
    @PostConstruct
    public void createMQTTServer(){
        ThreadUtil.execAsync(()->{
            new MQTTServer().bind(MQTT_SERVER_PORT);
        });
    }


    @Order(6)
    @PostConstruct
    public void createTCPClient(){
        new TCPClient().connect(host,TCP_SERVER_PORT);
    }

}
