package com.youlai.netty;

import cn.hutool.core.date.DateTime;
import cn.hutool.core.thread.ThreadUtil;
import cn.hutool.core.util.RandomUtil;
import cn.hutool.json.JSONUtil;
import com.youlai.netty.service.tcp.MyMessageProtocol;
import com.youlai.netty.service.tcp.client.TCPClient;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

/**
 * @author zc
 * @date 2023-02-04 15:23
 */
@SpringBootTest
@Slf4j
public class NettyApplicationTests {

    @Test
    void contextLoads() {
        for (int i = 0; i < 200; i++) {
            new TCPClient().connect("127.0.0.1",5600);
            log.info("连接成功：{}",i);
        }
    }

    /**
     * 生成测试消息
     */
    @Test
    void generateMessageTest(){
        MyMessageProtocol protocol = new MyMessageProtocol();
        protocol.setId(1);
        protocol.setBody(RandomUtil.randomString(10));
        protocol.setTime(DateTime.now().toStringDefaultTimeZone());
        System.out.println(JSONUtil.toJsonPrettyStr(protocol));
    }
}
