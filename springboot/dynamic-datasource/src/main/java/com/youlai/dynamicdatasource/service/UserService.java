package com.youlai.dynamicdatasource.service;

import com.youlai.dynamicdatasource.model.User;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 用户接口
 *
 * @author Ray
 * @since 2024-09-11 00:07:21
 */
public interface UserService extends IService<User> {

}
