package com.youlai.dynamicdatasource.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.youlai.dynamicdatasource.model.User;
import com.youlai.dynamicdatasource.service.UserService;
import com.youlai.dynamicdatasource.mapper.UserMapper;
import org.springframework.stereotype.Service;

/**
 * 用户服务实现类
 *
 * @author Ray
 * @since 0.0.1
 */
@Service
public class UserServiceImpl extends ServiceImpl<UserMapper, User>
        implements UserService {

}




