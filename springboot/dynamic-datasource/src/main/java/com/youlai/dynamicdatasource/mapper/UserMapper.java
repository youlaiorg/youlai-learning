package com.youlai.dynamicdatasource.mapper;

import com.youlai.dynamicdatasource.model.User;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;


@Mapper
public interface UserMapper extends BaseMapper<User> {

}




