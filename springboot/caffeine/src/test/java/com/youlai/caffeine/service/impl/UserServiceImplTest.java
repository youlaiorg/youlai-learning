package com.youlai.caffeine.service.impl;

import com.youlai.caffeine.dao.UserRepository;
import com.youlai.caffeine.domain.User;
import org.databene.contiperf.PerfTest;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Collections;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class UserServiceImplTest {

    @Mock
    private UserRepository mockUserRepository;

    @InjectMocks
    private UserServiceImpl userServiceImplUnderTest;

    @Test
    @PerfTest(invocations = 1000, threads = 20)  //设置性能测试方法执行的次数和并发线程数
    void testGetUserById() {
        // Setup
        when(mockUserRepository.findById(0L)).thenReturn(new User(0L, "name", "role"));

        // Run the test
        final User result = userServiceImplUnderTest.getUserById(0L);

        // Verify the results
    }

    @Test
    void testGetUsersByRole() {
        // Setup
        when(mockUserRepository.getUsersByRole("role")).thenReturn(List.of(new User(0L, "name", "role")));

        // Run the test
        final List<User> result = userServiceImplUnderTest.getUsersByRole("role");

        // Verify the results
    }

    @Test
    void testGetUsersByRole_UserRepositoryReturnsNoItems() {
        // Setup
        when(mockUserRepository.getUsersByRole("role")).thenReturn(Collections.emptyList());

        // Run the test
        final List<User> result = userServiceImplUnderTest.getUsersByRole("role");

        // Verify the results
        assertThat(result).isEqualTo(Collections.emptyList());
    }

    @Test
    void testSaveUser() {
        // Setup
        final User user = new User(0L, "name", "role");
        when(mockUserRepository.save(any(User.class))).thenReturn(new User(0L, "name", "role"));

        // Run the test
        final User result = userServiceImplUnderTest.saveUser(user);

        // Verify the results
    }

    @Test
    void testDeleteUserById() {
        // Setup
        // Run the test
        userServiceImplUnderTest.deleteUserById(0L);

        // Verify the results
        verify(mockUserRepository).deleteById(0L);
    }
}
