package com.youlai.caffeine.service;

import com.youlai.caffeine.domain.User;

import java.util.List;

/**
 * 用户业务层
 * @author zc
 * @date 2023-03-28 23:54
 */
public interface UserService {

        User getUserById(Long id);

        List<User> getUsersByRole(String role);

        User saveUser(User user);

        void deleteUserById(Long id);
}
