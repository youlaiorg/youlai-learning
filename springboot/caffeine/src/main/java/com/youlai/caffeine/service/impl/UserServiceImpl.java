package com.youlai.caffeine.service.impl;

import com.youlai.caffeine.dao.UserRepository;
import com.youlai.caffeine.domain.User;
import com.youlai.caffeine.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * 用户业务层
 * @author zc
 * @date 2023-03-28 23:52
 */
@Service
@Slf4j
public class UserServiceImpl implements UserService {

    @Resource
    private UserRepository userRepository;

    @Override
    @Cacheable(value = "users", keyGenerator = "customCacheKeyGenerator")
    public User getUserById(Long id) {
        log.info("业务层查询用户");
        User user = userRepository.findById(id);
        return user;
    }

    @Override
    @Cacheable(value = "users", keyGenerator = "customCacheKeyGenerator")
    public List<User> getUsersByRole(String role) {
        log.info("业务层查询用户");
        List<User> users = userRepository.getUsersByRole(role);
        return users;
    }

    @Override
    @CachePut(value = "users", keyGenerator = "customCacheKeyGenerator")
    public User saveUser(User user) {
        log.info("业务层保存用户");
        User save = userRepository.save(user);
        return save;
    }

    @Override
    @CacheEvict(value = "users", keyGenerator = "customCacheKeyGenerator")
    public void deleteUserById(Long id) {
        log.info("业务层删除用户");
        userRepository.deleteById(id);
    }


}
