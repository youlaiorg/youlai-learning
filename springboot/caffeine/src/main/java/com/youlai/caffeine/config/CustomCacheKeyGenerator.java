package com.youlai.caffeine.config;

import lombok.extern.slf4j.Slf4j;
import org.springframework.cache.interceptor.KeyGenerator;
import org.springframework.cache.interceptor.SimpleKey;
import org.springframework.stereotype.Component;

import java.lang.reflect.Method;

/**
 * 自定义缓存key生成器
 * @author zc
 * @date 2023-03-28 23:52
 */
@Component
@Slf4j
public class CustomCacheKeyGenerator implements KeyGenerator {

    @Override
    public Object generate(Object target, Method method, Object... params) {
        if (method.getName().equals("getUserById")) {
            log.info("生成缓存key: user_" + params[0] + "");
            return "user_" + params[0];
        }
        if (method.getName().equals("getUsersByRole")) {
            log.info("生成缓存key: role_" + params[0] + "");
            return "role_" + params[0];
        }
        return SimpleKey.EMPTY;
    }
}