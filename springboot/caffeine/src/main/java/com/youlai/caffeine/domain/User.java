package com.youlai.caffeine.domain;

/**
 * 用户实体类
 * @author zc
 * @date 2023-03-28 23:58
 */
public class User {

        private Long id;
        private String name;
        private String role;

        public User() {
        }

        public User(Long id, String name, String role) {
            this.id = id;
            this.name = name;
            this.role = role;
        }

        public Long getId() {
            return id;
        }

        public void setId(Long id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getRole() {
            return role;
        }

        public void setRole(String role) {
            this.role = role;
        }
}
