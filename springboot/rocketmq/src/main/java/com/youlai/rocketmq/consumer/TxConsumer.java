package com.youlai.rocketmq.consumer;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.rocketmq.spring.annotation.RocketMQMessageListener;
import org.apache.rocketmq.spring.core.RocketMQListener;
import org.springframework.stereotype.Component;

/**
 * 事务消息消费者
 * @author zc
 * @date 2023-01-27 03:09
 */
@Component
@RocketMQMessageListener(topic = "tx", consumerGroup = "tx_group",selectorExpression = "tx_expression")
@Slf4j
@RequiredArgsConstructor
public class TxConsumer implements RocketMQListener<String> {


    @Override
    public void onMessage(String message) {
      log.info("事务消息-接收到消息:"+message);
    }
}


