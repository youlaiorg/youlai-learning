package com.youlai.rocketmq.pojo.form;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author zc
 * @date 2022-11-29 06:55
 */
@ApiModel("顺序消息对象")
@Data
public class OrderForm {

    @ApiModelProperty("消息1")
    private String create;

    @ApiModelProperty("消息2")
    private String pay;

    @ApiModelProperty("消息3")
    private String deliver;
}
