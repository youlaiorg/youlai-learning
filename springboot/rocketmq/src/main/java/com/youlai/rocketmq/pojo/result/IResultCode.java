package com.youlai.rocketmq.pojo.result;

/**
 * @author Ray
 **/
public interface IResultCode {

    String getCode();

    String getMsg();

}
