package com.youlai.rocketmq.controller;

import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import com.youlai.rocketmq.pojo.form.*;
import com.youlai.rocketmq.pojo.result.Result;
import com.youlai.rocketmq.producer.*;
import com.youlai.rocketmq.producer.tx.TxProducer;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * @author zc
 * @date 2022-10-27 01:01
 */

@Api(tags = "RocketMQ接口")
@RestController
@RequestMapping("/api/v1/rocketmq")
public class RocketMQController {


    /**
     * 普通信息
     */
    @Resource
    private BaseProducer baseProducer;

    /**
     * 顺序消息发送样例
     */
    @Resource
    private OrderProducer orderProducer;

    /**
     * 延时消息
     */
    @Resource
    private ScheduledProducer scheduledProducer;

    /**
     * 批量消息
     */
    @Resource
    private BatchProducer batchProducer;

    /**
     * 事务消息
     */
    @Resource
    private TxProducer txProducer;


    @ApiOperation(value = "普通同步消息", notes = "普通同步消息")
    @ApiOperationSupport(order = 1)
    @PostMapping("/base/sync")
    public Result baseSync(String message) {
        boolean bool = baseProducer.sync(message);
        return Result.success(bool?"发送成功":"发送失败");
    }

    @ApiOperation(value = "普通异步消息", notes = "普通异步消息")
    @ApiOperationSupport(order = 5)
    @PostMapping("/base/async")
    public Result baseAsync(String message) {
        boolean bool =baseProducer.async(message);
        return Result.success(bool?"发送成功":"发送失败");
    }

    @ApiOperation(value = "普通单向消息", notes = "普通单向消息")
    @ApiOperationSupport(order = 10)
    @PostMapping("/base/oneway")
    public Result baseOneway(String message) {
        boolean bool =baseProducer.oneWay(message);
        return Result.success(bool?"发送成功":"发送失败");
    }

    @ApiOperation(value = "顺序消息", notes = "顺序消息")
    @ApiOperationSupport(order = 15)
    @PostMapping("/order")
    public Result order(@RequestBody OrderForm orderForm) {
        orderProducer.order(orderForm.getCreate(),orderForm.getPay(),orderForm.getDeliver());
        return Result.success("发送成功");
    }


    @ApiOperation(value = "延时消息", notes = "延时消息")
    @ApiOperationSupport(order = 20)
    @PostMapping("/scheduled")
    public Result scheduled(@RequestBody ScheduledForm scheduledForm) {
        scheduledProducer.scheduled(scheduledForm.getDelayLevel(),scheduledForm.getBody());
        return Result.success("发送成功");
    }

    @ApiOperation(value = "批量消息", notes = "批量消息")
    @ApiOperationSupport(order = 25)
    @PostMapping("/batch")
    public Result batch(@RequestBody BatchForm batchForm) {
        // 批量消息样例
        batchProducer.batch(batchForm.getMessage1(),batchForm.getMessage2(),batchForm.getMessage3(),batchForm.getMessage4(),batchForm.getMessage5());
        return Result.success("发送成功");
    }

    @ApiOperation(value = "事务消息", notes = "事务消息")
    @ApiOperationSupport(order = 30)
    @PostMapping("/tx")
    public Result tx(Boolean isOpenTx) {
        txProducer.tx(isOpenTx);
        return Result.success("发送成功");
    }

}

