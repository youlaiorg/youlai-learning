package com.youlai.rocketmq.service;

/**
 * rocketmq常见问题
 * @author zc
 * @date 2023-01-27 16:10
 */
public interface RocketmqCommonProblemService {
    void commonProblem();
}
