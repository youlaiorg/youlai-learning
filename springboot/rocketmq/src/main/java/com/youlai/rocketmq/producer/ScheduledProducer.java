package com.youlai.rocketmq.producer;

import lombok.extern.slf4j.Slf4j;
import org.apache.rocketmq.client.producer.SendCallback;
import org.apache.rocketmq.client.producer.SendResult;
import org.apache.rocketmq.client.producer.SendStatus;
import org.apache.rocketmq.spring.core.RocketMQTemplate;
import org.springframework.messaging.Message;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * 延时消息生产者
 * @author zc
 * @date 2022-10-27 01:00
 */
@Service
@Slf4j
public class ScheduledProducer {
    @Resource
    RocketMQTemplate rocketMQTemplate;

    public void scheduled(Integer delayLevel,String body) {
        String text = "延时"+delayLevel+"消息:"+ body;
        log.info(text);
        // 设置延时等级2,这个消息将在5s之后发送
        // 1s 5s 10s 30s 1m 2m 3m 4m 5m 6m 7m 8m 9m 10m 20m 30m 1h 2h
        Message<String> message = MessageBuilder.withPayload(body).build();
        SendResult sendResult = rocketMQTemplate.syncSend("scheduled_topic", message, 1000, delayLevel);
        log.info("延时消息发送:"+sendResult.getSendStatus().toString());
    }
}
