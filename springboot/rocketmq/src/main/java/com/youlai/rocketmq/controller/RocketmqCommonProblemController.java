package com.youlai.rocketmq.controller;

import com.youlai.rocketmq.pojo.result.Result;
import com.youlai.rocketmq.service.RocketmqCommonProblemService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * rocketmq常见问题
 * @author zc
 * @date 2023-01-27 16:05
 */

@Api(tags = "RocketMQ常见问题测试接口")
@RestController
@RequestMapping("/api/v1/rocketmq/common/problem")
public class RocketmqCommonProblemController {

    private RocketmqCommonProblemService messageLose;

    public RocketmqCommonProblemController(RocketmqCommonProblemService messageLose) {
        this.messageLose = messageLose;
    }

    /**
     * 消息丢失
     * @return
     */
    @ApiOperation(value = "消息丢失")
    @GetMapping("/lose")
    public Result lose(){
         messageLose.commonProblem();
         return Result.success();
    }
}
