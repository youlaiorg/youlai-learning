package com.youlai.redis.service.exception;


/**
 * @Author zc
 * @Date 2021/3/26 19:54
 * @Description redis限流异常
 */
public class RedisLimitException extends RuntimeException{
 public RedisLimitException(String msg) {
  super( msg );
 }
}