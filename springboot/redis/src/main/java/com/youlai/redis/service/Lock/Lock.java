package com.youlai.redis.service.Lock;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 分布式锁注解
 * @Author zc
 * @Date 2021/3/26 19:54
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface Lock {
    /**
     * lock key
     */
    String value();

    /**
     * 锁超时时间，默认5000ms
     */
    long expireTime() default 5000L;

    /**
     * 锁等待时间，默认50ms
     */
    long waitTime() default 50L;
}
