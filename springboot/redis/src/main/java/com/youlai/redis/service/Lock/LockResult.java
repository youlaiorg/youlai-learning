package com.youlai.redis.service.Lock;

import lombok.Getter;
import lombok.Setter;
import org.redisson.api.RLock;

/**
 * @author zc
 * @date 2023-03-26 20:47
 */
@Getter
@Setter
public class LockResult {
    private LockResultStatus lockResultStatus;

    private RLock rLock;
}
