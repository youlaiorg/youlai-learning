package com.youlai.redis.service.Lock;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * @author zc
 * @date 2023-03-26 20:52
 */
@Service
@Slf4j
public class LockTestService {

    @Lock(value = "'test_'+#student.stuNo",expireTime = 3000, waitTime = 30)
    public String test(Student student) {
        // 业务代码
        log.info("进入业务方法");
        return student.getName();
    }
}
