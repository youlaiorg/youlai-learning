package com.youlai.redis.service.Lock;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author zc
 * @date 2023-03-26 20:53
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Student {
    private String stuNo;
    private String name;
}
