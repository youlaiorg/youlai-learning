package com.youlai.redis.controller;

import com.youlai.redis.service.Lock.LockTestService;
import com.youlai.redis.service.Lock.Student;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * 分布式锁
 * @author zc
 * @date 2023-03-26 23:24
 */
@Slf4j
@RestController
@RequestMapping("/lock")
public class LockController {

    @Resource
    private LockTestService lockTestService;
    /**
     * 基于Redis AOP分布式锁
     */
    @GetMapping("/test")
    public String test() {
        return lockTestService.test(new Student("1", "张三"));
    }
}
