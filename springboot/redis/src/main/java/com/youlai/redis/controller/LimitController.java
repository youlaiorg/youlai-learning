package com.youlai.redis.controller;

import com.youlai.redis.service.limitflow.RedisLimit;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDateTime;

/**
 * @author zc
 * @date 2023-03-26 20:10
 */
@Slf4j
@RestController
@RequestMapping("/limit")
public class LimitController {
    /**
     * 基于Redis AOP限流
     */
    @GetMapping("/test")
    @RedisLimit(key = "redis-limit:test", permitsPerSecond = 1, expire = 1, msg = "当前排队人数较多，请稍后再试！")
    public String test() {
        log.info(LocalDateTime.now()+"进入限流方法");
        return "ok";
    }
}
