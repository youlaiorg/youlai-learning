package com.youlai.redis.controller;

import com.youlai.redis.service.delayqueue.RedissonDelayQueue;
import jakarta.annotation.Resource;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;


/**
 * @author zc
 * @date 2023-03-26 23:48
 */
@Slf4j
@RestController
public class delayQueueController {

    @Resource
    RedissonDelayQueue redissonDelayQueue;



    @GetMapping("/sendMessage")
    public String getToken() {
        redissonDelayQueue.offerTask("hello,world",5);
        return "ok";
    }
}
