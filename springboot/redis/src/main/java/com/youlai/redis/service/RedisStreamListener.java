package com.youlai.redis.service;

import com.youlai.redis.config.RedisConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.redis.connection.stream.ObjectRecord;
import org.springframework.data.redis.stream.StreamListener;

/**
 * StreamListener 不同于发布订阅的是 同一个消费者组中只能有一个消费者监听到消息
 *
 * @author Fxz
 * @version 0.0.1
 * @date 2023/3/26 15:49
 */
public class RedisStreamListener implements StreamListener<String, ObjectRecord<String, String>> {

    private final Logger logger = LoggerFactory.getLogger(RedisConfig.class);

    @Override
    public void onMessage(ObjectRecord<String, String> message) {
        logger.info("接受到来自Stream的消息:{}", message.getValue());
    }

}
