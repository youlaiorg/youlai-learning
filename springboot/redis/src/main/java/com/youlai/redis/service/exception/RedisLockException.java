package com.youlai.redis.service.exception;

import com.youlai.redis.service.Lock.ResponseEnum;

/**
 * redis锁异常
 * @author zc
 * @date 2023-03-26 20:50
 */
public class RedisLockException extends RuntimeException{
    public RedisLockException(ResponseEnum msg) {
        super(String.valueOf(msg));
    }
}
