package com.youlai.redis;


import com.youlai.redis.service.AdvancedFeaturesService;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import javax.annotation.Resource;
import java.util.concurrent.CountDownLatch;


/**
 * redis高级特性
 */
@SpringBootTest
public class AdvancedFeaturesServiceTests {


    @Resource
    private AdvancedFeaturesService advancedFeaturesService;

    /**
     * 事务操作
     */
    @Test
    void testExecuteTransaction() {
        advancedFeaturesService.transactionalMethod(false);
    }


    /**
     * 管道操作
     */
    @Test
    void testpipelineExample() {
        advancedFeaturesService.pipelineExample();
    }


    /**
     * 发布
     */
    @Test
    public void testPubSub() {
        advancedFeaturesService.publish("hello word!");
    }

    /**
     * 发布 不同于发布订阅的是 同一个消费者组中只能有一个消费者监听到消息
     */
    @Test
    public void testStream() {
        advancedFeaturesService.publishStream("hello word!");
    }

    /**
     * lua脚本
     */
    @Test
    void testIncrement() throws InterruptedException {
        CountDownLatch latch = new CountDownLatch(10);
        for (int i = 0; i < 10; i++) {
            new Thread(() -> {
                System.out.println(advancedFeaturesService.increment("id"));
                latch.countDown();
            }).start();
        }
        latch.await();
    }

    /**
     * 过期时间
     */
    @Test
    void testExpirationTime() {
        advancedFeaturesService.expirationTime();
    }

}
