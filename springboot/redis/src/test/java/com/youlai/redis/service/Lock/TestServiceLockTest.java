package com.youlai.redis.service.Lock;

import org.databene.contiperf.PerfTest;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

//@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class TestServiceLockTest {

    private LockTestService testServiceUnderLockTest;

    @BeforeEach
    void setUp() {
        testServiceUnderLockTest = new LockTestService();
    }

    @Test
    @PerfTest(invocations = 1000, threads = 20)  //设置性能测试方法执行的次数和并发线程数
    void testTest() {
       testServiceUnderLockTest.test(new Student());
    }
}
