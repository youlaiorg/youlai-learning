package com.youlai.redis;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Range;
import org.springframework.data.redis.core.*;

import javax.annotation.Resource;

/**
 * 五种数据类型的基本增删改查
 */
@SpringBootTest
class RedisCRUDTests {
    @Resource
    private ReactiveRedisTemplate<String,String> reactiveRedisTemplate;

    /**
     * String类型
     */
    @Test
    void stringTest() {
        //插入数据
        reactiveRedisTemplate.opsForValue().set("key", "value");
        //查询数据
        reactiveRedisTemplate.opsForValue().get("key").subscribe();
        //更新数据
        reactiveRedisTemplate.opsForValue().set("key", "new_value");
        //删除数据
        reactiveRedisTemplate.delete("key");
    }

    /**
     * Hash类型
     */
    @Test
    void hashTest(){
        //插入数据
        ReactiveHashOperations<String, String, String> hashOps1 = reactiveRedisTemplate.opsForHash();
        hashOps1.put("hash_key", "field1", "value1").subscribe();
        hashOps1.put("hash_key", "field2", "value2").subscribe();
        //查询数据
        ReactiveHashOperations<String, String, String> hashOps2 = reactiveRedisTemplate.opsForHash();
        hashOps2.get("hash_key", "field1").subscribe();

        //更新数据
        ReactiveHashOperations<String, String, String> hashOps3 = reactiveRedisTemplate.opsForHash();
        hashOps3.put("hash_key", "field1", "new_value").subscribe();

        //删除数据
        ReactiveHashOperations<String, String, String> hashOps4 = reactiveRedisTemplate.opsForHash();
        hashOps4.delete("hash_key").subscribe();

    }


    /**
     * List类型
     */
    @Test
    void listTest(){
        //插入数据
        ReactiveListOperations<String, String> listOps1 = reactiveRedisTemplate.opsForList();
        listOps1.leftPush("list_key", "value1").subscribe();
        listOps1.leftPush("list_key", "value2").subscribe();
        listOps1.rightPush("list_key", "value3").subscribe();

        //查询数据
        ReactiveListOperations<String, String> listOps2 = reactiveRedisTemplate.opsForList();
        listOps2.range("list_key", 0, -1).subscribe();

        //更新数据
        ReactiveListOperations<String, String> listOps3 = reactiveRedisTemplate.opsForList();
        listOps3.set("list_key", 0, "new_value").subscribe();

        //删除数据
        ReactiveListOperations<String, String> listOps4 = reactiveRedisTemplate.opsForList();
        listOps4.trim("list_key", 0, -2).subscribe();
    }

    /**
     * Set类型
     */
    @Test
    void setTest(){
        //插入数据
        ReactiveSetOperations<String, String> setOps1 = reactiveRedisTemplate.opsForSet();
        setOps1.add("set_key", "value1").subscribe();
        setOps1.add("set_key", "value2").subscribe();

        //查询数据
        ReactiveSetOperations<String, String> setOps2 = reactiveRedisTemplate.opsForSet();
        setOps2.members("set_key").subscribe();

        //更新数据
        ReactiveSetOperations<String, String> setOps3 = reactiveRedisTemplate.opsForSet();
        setOps3.add("set_key", "new_value").subscribe();

        //删除数据
        ReactiveSetOperations<String, String> setOps4 = reactiveRedisTemplate.opsForSet();
        setOps4.remove("set_key", "value1").subscribe();
    }


    /**
     * ZSet类型
     */
    @Test
    void zsetTest(){
        //插入数据
        ReactiveZSetOperations<String, String> zSetOps1 = reactiveRedisTemplate.opsForZSet();
        zSetOps1.add("zset_key", "value1", 1).subscribe();
        zSetOps1.add("zset_key", "value2", 2).subscribe();

        //查询数据
        ReactiveZSetOperations<String, String> zSetOps2 = reactiveRedisTemplate.opsForZSet();
        zSetOps2.range("zset_key", Range.<Long>from(Range.Bound.inclusive(0l)).to(Range.Bound.inclusive(-1l))).subscribe();

        //更新数据
        ReactiveZSetOperations<String, String> zSetOps3 = reactiveRedisTemplate.opsForZSet();
        zSetOps3.add("zset_key", "value1", 3).subscribe();

        //删除数据
        ReactiveZSetOperations<String, String> zSetOps4 = reactiveRedisTemplate.opsForZSet();
        zSetOps4.remove("zset_key", "value1").subscribe();

    }

}
